Personal fork of slstatus.

Patches:
- Custom patching to display `01`, `02`, etc. if stat percentages for CPU, RAM, or swap are less than `10`.
